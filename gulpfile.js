const gulp = require("gulp");
const sass = require("gulp-ruby-sass");
const pug = require("gulp-pug");
const watch = require("gulp-watch");
const autoprefixer = require("gulp-autoprefixer");
const connect = require("gulp-connect");
const concat = require("gulp-concat");
const minify = require("gulp-minify");

gulp.task("watch", () => {
  gulp.watch("src/sass/**/*.scss", ["sass"]);
  gulp.watch("src/**/*.pug", ["pug"]);
  gulp.watch("src/js/**/*.js", ["js-concat"]);
  // gulp.watch('src/js/**/*.js', ['js-compress']);
});

gulp.task("sass", () => {
  sass("src/sass/*.scss", {
    sourcemap: true,
    style: "compressed"
  })
    .on("error", sass.logError)
    .pipe(
      autoprefixer({
        browsers: ["last 5 versions"],
        cascade: false
      })
    )
    .pipe(gulp.dest("assets/css"))
    .pipe(connect.reload());
});

gulp.task("pug", () => {
  return gulp
    .src("src/pug/*.pug")
    .pipe(
      pug({
        pretty: true
      })
    )
    .pipe(gulp.dest("./"))
    .pipe(connect.reload());
});

gulp.task("js-concat", () => {
  return gulp
    .src(["./src/js/vendor/*.js", "./src/js/partials/*.js"])
    .pipe(concat("all.js"))
    .pipe(minify())
    .pipe(gulp.dest("./assets/js"))
    .pipe(connect.reload());
});


gulp.task("connect", () => {
  connect.server({
    port: 8000,
    root: "./",
    livereload: true
  });
});

gulp.task("default", ["connect", "watch"]);
