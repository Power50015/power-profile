$(function() {
  $("body").niceScroll({
    cursorcolor: "#F2424C", // change cursor color in hex
    cursoropacitymin: 1, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
    cursorborder: "none", // css definition for cursor border
    cursorborderradius: "1px", // border radius in pixel for cursor
    zindex: "9999", // change z-index for scrollbar div
    scrollspeed: 10, // scrolling speed
    mousescrollstep: 10, // scrolling speed with mouse wheel (pixel)
  });
  
});
